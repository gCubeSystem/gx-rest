package org.gcube.common.gxhttp;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.Properties;
import java.util.WeakHashMap;

import org.gcube.common.gxhttp.request.GXHTTPStringRequest;
import org.gcube.common.gxhttp.util.ContentUtils;
import org.junit.Before;
import org.junit.Test;


/**
 * Test cases for {@link GXHTTPStringRequest}
 * 
 * @author Manuele Simi (ISTI-CNR)
 *
 */
public class GXHTTPStringRequestTest {

	private GXHTTPStringRequest request;
	
	public static String DEFAULT_TEST_SCOPE = "";
	
	static String DEFAULT_RM_URL = "";

	static String DEFAULT_RR_URL = "";

	
	private static boolean skipTest = false;

	static {
		Properties properties = new Properties();
		try (InputStream input = GXHTTPStringRequestTest.class.getClassLoader().getResourceAsStream("token.props")) {
			// load the properties file
			properties.load(input);
			DEFAULT_TEST_SCOPE = properties.getProperty("DEFAULT_SCOPE_TOKEN");
			if (DEFAULT_TEST_SCOPE.isEmpty())
				skipTest = true;
			DEFAULT_RM_URL = properties.getProperty("RM_URL");
			DEFAULT_RR_URL = properties.getProperty("RR_URL");
		} catch (IOException | NullPointerException e) {
			skipTest = true;
		}
	}

	/**
	 * Test method for {@link org.gcube.common.gxhttp.request.GXHTTPStringRequest#newRequest(java.lang.String)}.
	 */
	@Before
	public void testNewRequest() {
		request  = GXHTTPStringRequest.newRequest(DEFAULT_RM_URL).from("GXRequestTest");
	}

	/**
	 * Test method for {@link org.gcube.common.gxhttp.request.GXHTTPStringRequest#post(java.lang.String)}.
	 */
	@Test
	public void testPostString() {
		if (skipTest)
			return;
		request.clear();
		String context ="{\"@class\":\"Context\",\"header\":{\"@class\":\"Header\",\"uuid\":\"6f86dc81-2f59-486b-8aa9-3ab5486313c4\",\"creator\":null,\"modifiedBy\":\"gxRestTest\",\"creationTime\":null,\"lastUpdateTime\":null},\"name\":\"gxTest\",\"parent\":null,\"children\":[]}";
		Map<String,String> queryParams = new WeakHashMap<>();
		queryParams.put("rrURL", DEFAULT_RR_URL);
		try {
			 HttpURLConnection response = request.path("gxrest")
					.header("Another header", "GXHTTPRequestTest")
					.queryParams(queryParams).post(context);
			assertTrue("Unexpected returned code.", response.getResponseCode() == 200);
			String body =  ContentUtils.toString(ContentUtils.toByteArray(response.getInputStream()));
			System.out.println("Returned string " + body);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail("Failed to send a POST request");
		}	
		
	}

}
